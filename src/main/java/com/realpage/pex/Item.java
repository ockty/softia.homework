package com.realpage.pex;

import java.time.LocalDate;

import static java.time.temporal.ChronoUnit.DAYS;

public class Item {

    LocalDate start;
    LocalDate end;
    Schedule schedule;

    public Item(LocalDate start, LocalDate end, Schedule schedule) {
        this.start = start;
        this.end = end;
        this.schedule = schedule;
    }

    public LocalDate getStart() {
        return start;
    }

    public void setStart(LocalDate start) {
        this.start = start;
    }

    public LocalDate getEnd() {
        return end;
    }

    public void setEnd(LocalDate end) {
        this.end = end;
    }

    public Schedule getSchedule() {
        return schedule;
    }

    public void setSchedule(Schedule schedule) {
        this.schedule = schedule;
    }

    public int getDaysInPeriod() {
        return (int) DAYS.between(start, end) + 1;
    }


    @Override
    public String toString() {
        return "Item{" +
                "start=" + start +
                ", end=" + end +
                ", schedule=" + schedule +
                '}';
    }
}