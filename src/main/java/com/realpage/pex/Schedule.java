package com.realpage.pex;

/**
 * Created by paul on 10/18/2020.
 */
public class Schedule {

    String rate;
    String rateFrequencyString;
    String startDate;
    String endDate;
    String billingFrequency;

    private Schedule(Builder builder) {
        setRate(builder.rate);
        setRateFrequencyString(builder.rateFrequencyString);
        setStartDate(builder.startDate);
        setEndDate(builder.endDate);
        setBillingFrequency(builder.billingFrequency);
    }

    public static Builder newBuilder() {
        return new Builder();
    }


    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getRateFrequencyString() {
        return rateFrequencyString;
    }

    public void setRateFrequencyString(String rateFrequencyString) {
        this.rateFrequencyString = rateFrequencyString;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getBillingFrequency() {
        return billingFrequency;
    }

    public void setBillingFrequency(String billingFrequency) {
        this.billingFrequency = billingFrequency;
    }


    public static final class Builder {
        private String rate;
        private String rateFrequencyString;
        private String startDate;
        private String endDate;
        private String billingFrequency;

        private Builder() {
        }

        public Builder withRate(String val) {
            rate = val;
            return this;
        }

        public Builder withRateFrequencyString(String val) {
            rateFrequencyString = val;
            return this;
        }

        public Builder withStartDate(String val) {
            startDate = val;
            return this;
        }

        public Builder withEndDate(String val) {
            endDate = val;
            return this;
        }

        public Builder withBillingFrequency(String val) {
            billingFrequency = val;
            return this;
        }

        public Schedule build() {
            return new Schedule(this);
        }
    }

    @Override
    public String toString() {
        return "Schedule{" +
                "rate='" + rate + '\'' +
                ", rateFrequencyString='" + rateFrequencyString + '\'' +
                ", startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                ", billingFrequency='" + billingFrequency + '\'' +
                '}';
    }
}
