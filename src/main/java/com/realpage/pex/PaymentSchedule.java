package com.realpage.pex;

import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class PaymentSchedule {

    public static final String QUARTERLY = "Quarterly";
    public static final String WEEKLY = "Weekly";


    private DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("d MMM u", Locale.ENGLISH);
    private Schedule schedule;

    public PaymentSchedule(Schedule schedule) {
        this.schedule = schedule;
    }


    public List<Item> getItems() {
        LocalDate originalStartDate = LocalDate.parse(schedule.getStartDate(), dateFormatter);
        LocalDate originalEndDate = LocalDate.parse(schedule.getEndDate(), dateFormatter);
        return createItemList(originalStartDate, originalEndDate);

    }


    private List<Item> createItemList(LocalDate originalStartDate, LocalDate originalEndDate) {
        LocalDate startDate;
        LocalDate endDate;
        List<Item> items = new ArrayList<>();

        for (int month = 0; month < Month.values().length; month++) {
            if (QUARTERLY.equalsIgnoreCase(schedule.getBillingFrequency())) {
                startDate = originalStartDate.plusMonths(month);
                endDate = originalStartDate.plusMonths(month + 3).minusDays(1);
                month += 2;
            } else {
                startDate = originalStartDate.plusMonths(month);
                endDate = originalStartDate.plusMonths(month + 1).minusDays(1);
            }
            if (endDate.isBefore(originalEndDate) || endDate.equals(originalEndDate)) {
                items.add(new Item(startDate, endDate, schedule));
            }
        }
        return items;
    }
}
