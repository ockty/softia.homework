import com.realpage.pex.Item;
import com.realpage.pex.PaymentSchedule;
import com.realpage.pex.Schedule;
import org.junit.Assert;
import org.junit.Test;

import java.text.ParseException;
import java.util.List;

import static com.realpage.pex.PaymentSchedule.QUARTERLY;
import static com.realpage.pex.PaymentSchedule.WEEKLY;
import static java.time.temporal.ChronoUnit.DAYS;


/**
 * Create a payment schedule to generate rental payment schedule
 * <p>
 * Each test case has example out put in the comments
 * <p>
 * Test cases:
 * #. Rent, Rent Rate, Start, End, Billing Frequency
 * 1. £1350, Monthly, 1 Feb 2016, 31 Jan 2017, Monthly
 * 2. £1350, Monthly, 15 Feb 2016, 14 Feb 2017, Monthly
 * 3. £1350, Monthly, 1 Feb 2016, 31 Dec 2016, Monthly
 * 4. £1350, Monthly, 30 Jan 2016, 29 Jan 2017, Monthly
 * 5. £1350, Monthly, 31 Jan 2016, 30 Jan 2017, Monthly
 * 6. £350, Weekly, 1 Jan 2016, 31 Dec 2016, Monthly
 * 7. £1350, Monthly, 15 Feb 2016, 14 Feb 2017, Quarterly
 */


public class DevTestJunitTest {

    private PaymentSchedule getSchedule(String rate, String rateFrequencyString, String startDate, String endDate, String billingFrequency) {
        Schedule schedule = Schedule.newBuilder()
                .withRate(rate)
                .withRateFrequencyString(rateFrequencyString)
                .withStartDate(startDate)
                .withEndDate(endDate)
                .withBillingFrequency(billingFrequency).build();

        PaymentSchedule paymentSchedule = new PaymentSchedule(schedule);
        return paymentSchedule;
    }

    private String getRate(Item item) {
        if (QUARTERLY.equalsIgnoreCase(item.getSchedule().getBillingFrequency())) {
            return getFormattedString(Double.valueOf(item.getSchedule().getRate()) * 3);
        }
        if (WEEKLY.equalsIgnoreCase(item.getSchedule().getRateFrequencyString())) {
            Double dayPrice = Double.valueOf(item.getSchedule().getRate()) / 7;
            return getFormattedString(item.getDaysInPeriod() * dayPrice);
        }
        return item.getSchedule().getRate();
    }


    private String getTotalCharge(PaymentSchedule paymentSchedule) {
        Double result = 0.0;
        Schedule schedule = paymentSchedule.getItems().get(0).getSchedule();

        if (QUARTERLY.equalsIgnoreCase(schedule.getBillingFrequency())) {
            result = Double.valueOf(schedule.getRate()) * 12;
        }
        else if (WEEKLY.equalsIgnoreCase(schedule.getRateFrequencyString())) {
            result = getTotalChargeForWeeklyRateFrequency(paymentSchedule, schedule);
        } else {
            for (Item item : paymentSchedule.getItems()) {
                result += Double.valueOf(item.getSchedule().getRate());
            }
        }
        return getFormattedString(result);
    }

    private Double getTotalChargeForWeeklyRateFrequency(PaymentSchedule paymentSchedule, Schedule schedule) {
        Double result;
        Double dayPrice = Double.valueOf(schedule.getRate()) / 7;
        Long totalDays = DAYS.between(paymentSchedule.getItems().get(0).getStart(), paymentSchedule.getItems().get(11).getEnd()) + 1;
        result = dayPrice * totalDays;
        return result;
    }

    private String getFormattedString(Double asDouble) {
        return String.format("%.2f", asDouble);
    }


    /**
     * Mon Feb 01 00:00:00 GMT 2016 Mon Feb 29 23:59:59 GMT 2016 1350.00 29
     * Tue Mar 01 00:00:00 GMT 2016 Thu Mar 31 23:59:59 BST 2016 1350.00 31
     * Fri Apr 01 00:00:00 BST 2016 Sat Apr 30 23:59:59 BST 2016 1350.00 30
     * Sun May 01 00:00:00 BST 2016 Tue May 31 23:59:59 BST 2016 1350.00 31
     * Wed Jun 01 00:00:00 BST 2016 Thu Jun 30 23:59:59 BST 2016 1350.00 30
     * Fri Jul 01 00:00:00 BST 2016 Sun Jul 31 23:59:59 BST 2016 1350.00 31
     * Mon Aug 01 00:00:00 BST 2016 Wed Aug 31 23:59:59 BST 2016 1350.00 31
     * Thu Sep 01 00:00:00 BST 2016 Fri Sep 30 23:59:59 BST 2016 1350.00 30
     * Sat Oct 01 00:00:00 BST 2016 Mon Oct 31 23:59:59 GMT 2016 1350.00 31
     * Tue Nov 01 00:00:00 GMT 2016 Wed Nov 30 23:59:59 GMT 2016 1350.00 30
     * Thu Dec 01 00:00:00 GMT 2016 Sat Dec 31 23:59:59 GMT 2016 1350.00 31
     * Sun Jan 01 00:00:00 GMT 2017 Tue Jan 31 23:59:59 GMT 2017 1350.00 31
     */
    @Test
    public void testCaseOne() throws ParseException {

        String rentAmount = "1350.00";

        PaymentSchedule schedule = getSchedule(rentAmount,"Monthly", "01 Feb 2016","31 Jan 2017","Monthly");

        List<Item> items = schedule.getItems();
        for (Item item : items) {
            System.out.println(item.getStart() + " " + item.getEnd() + " " + getRate(item) + " " + item.getDaysInPeriod());
        }

        Assert.assertEquals(12, items.size());

        //Feb
        Assert.assertEquals(29, items.get(0).getDaysInPeriod());
        Assert.assertEquals(rentAmount, getRate(items.get(0)));

        //March
        Assert.assertEquals(31, items.get(1).getDaysInPeriod());
        Assert.assertEquals(rentAmount, getRate(items.get(1)));

        //April
        Assert.assertEquals(30, items.get(2).getDaysInPeriod());
        Assert.assertEquals(rentAmount, getRate(items.get(2)));

        //May
        Assert.assertEquals(31, items.get(3).getDaysInPeriod());
        Assert.assertEquals(rentAmount, getRate(items.get(3)));

        //June
        Assert.assertEquals(30, items.get(4).getDaysInPeriod());
        Assert.assertEquals(rentAmount, getRate(items.get(4)));

        //July
        Assert.assertEquals(31, items.get(5).getDaysInPeriod());
        Assert.assertEquals(rentAmount, getRate(items.get(5)));

        //Aug
        Assert.assertEquals(31, items.get(6).getDaysInPeriod());
        Assert.assertEquals(rentAmount, getRate(items.get(6)));

        //Sep
        Assert.assertEquals(30, items.get(7).getDaysInPeriod());
        Assert.assertEquals(rentAmount, getRate(items.get(7)));

        //Oct
        Assert.assertEquals(31, items.get(8).getDaysInPeriod());
        Assert.assertEquals(rentAmount, getRate(items.get(8)));

        //Nov
        Assert.assertEquals(30, items.get(9).getDaysInPeriod());
        Assert.assertEquals(rentAmount, getRate(items.get(9)));

        //Dec
        Assert.assertEquals(31, items.get(10).getDaysInPeriod());
        Assert.assertEquals(rentAmount, getRate(items.get(10)));

        //Jan
        Assert.assertEquals(31, items.get(11).getDaysInPeriod());
        Assert.assertEquals(rentAmount, getRate(items.get(11)));

        Assert.assertEquals("16200.00", getTotalCharge(schedule));
    }

    /**
     * Mon Feb 15 00:00:00 GMT 2016 Mon Mar 14 23:59:59 GMT 2016 1350.00 29
     * Tue Mar 15 00:00:00 GMT 2016 Thu Apr 14 23:59:59 BST 2016 1350.00 31
     * Fri Apr 15 00:00:00 BST 2016 Sat May 14 23:59:59 BST 2016 1350.00 30
     * Sun May 15 00:00:00 BST 2016 Tue Jun 14 23:59:59 BST 2016 1350.00 31
     * Wed Jun 15 00:00:00 BST 2016 Thu Jul 14 23:59:59 BST 2016 1350.00 30
     * Fri Jul 15 00:00:00 BST 2016 Sun Aug 14 23:59:59 BST 2016 1350.00 31
     * Mon Aug 15 00:00:00 BST 2016 Wed Sep 14 23:59:59 BST 2016 1350.00 31
     * Thu Sep 15 00:00:00 BST 2016 Fri Oct 14 23:59:59 BST 2016 1350.00 30
     * Sat Oct 15 00:00:00 BST 2016 Mon Nov 14 23:59:59 GMT 2016 1350.00 31
     * Tue Nov 15 00:00:00 GMT 2016 Wed Dec 14 23:59:59 GMT 2016 1350.00 30
     * Thu Dec 15 00:00:00 GMT 2016 Sat Jan 14 23:59:59 GMT 2017 1350.00 31
     * Sun Jan 15 00:00:00 GMT 2017 Tue Feb 14 23:59:59 GMT 2017 1350.00 31
     */
    @Test
    public void testCaseTwo() throws ParseException {

        String rentAmount = "1350.00";

        PaymentSchedule schedule = getSchedule(rentAmount,"Monthly", "15 Feb 2016","14 Feb 2017","Monthly");

        List<Item> items = schedule.getItems();
        for (Item item : items) {
            System.out.println(item.getStart() + " " + item.getEnd() + " " + getRate(item) + " " + item.getDaysInPeriod());
        }

        Assert.assertEquals(12, items.size());

        //Feb
        Assert.assertEquals(29, items.get(0).getDaysInPeriod());
        Assert.assertEquals(rentAmount, getRate(items.get(0)));

        //March
        Assert.assertEquals(31, items.get(1).getDaysInPeriod());
        Assert.assertEquals(rentAmount, getRate(items.get(1)));

        //April
        Assert.assertEquals(30, items.get(2).getDaysInPeriod());
        Assert.assertEquals(rentAmount, getRate(items.get(2)));

        //May
        Assert.assertEquals(31, items.get(3).getDaysInPeriod());
        Assert.assertEquals(rentAmount, getRate(items.get(3)));

        //June
        Assert.assertEquals(30, items.get(4).getDaysInPeriod());
        Assert.assertEquals(rentAmount, getRate(items.get(4)));

        //July
        Assert.assertEquals(31, items.get(5).getDaysInPeriod());
        Assert.assertEquals(rentAmount, getRate(items.get(5)));

        //Aug
        Assert.assertEquals(31, items.get(6).getDaysInPeriod());
        Assert.assertEquals(rentAmount, getRate(items.get(6)));

        //Sep
        Assert.assertEquals(30, items.get(7).getDaysInPeriod());
        Assert.assertEquals(rentAmount, getRate(items.get(7)));

        //Oct
        Assert.assertEquals(31, items.get(8).getDaysInPeriod());
        Assert.assertEquals(rentAmount, getRate(items.get(8)));

        //Nov
        Assert.assertEquals(30, items.get(9).getDaysInPeriod());
        Assert.assertEquals(rentAmount, getRate(items.get(9)));

        //Dec
        Assert.assertEquals(31, items.get(10).getDaysInPeriod());
        Assert.assertEquals(rentAmount, getRate(items.get(10)));

        //Jan
        Assert.assertEquals(31, items.get(11).getDaysInPeriod());
        Assert.assertEquals(rentAmount, getRate(items.get(11)));

        Assert.assertEquals("16200.00", getTotalCharge(schedule));
    }

    /**
     * Mon Feb 01 00:00:00 GMT 2016 Mon Feb 29 23:59:59 GMT 2016 1350.00 29
     * Tue Mar 01 00:00:00 GMT 2016 Thu Mar 31 23:59:59 BST 2016 1350.00 31
     * Fri Apr 01 00:00:00 BST 2016 Sat Apr 30 23:59:59 BST 2016 1350.00 30
     * Sun May 01 00:00:00 BST 2016 Tue May 31 23:59:59 BST 2016 1350.00 31
     * Wed Jun 01 00:00:00 BST 2016 Thu Jun 30 23:59:59 BST 2016 1350.00 30
     * Fri Jul 01 00:00:00 BST 2016 Sun Jul 31 23:59:59 BST 2016 1350.00 31
     * Mon Aug 01 00:00:00 BST 2016 Wed Aug 31 23:59:59 BST 2016 1350.00 31
     * Thu Sep 01 00:00:00 BST 2016 Fri Sep 30 23:59:59 BST 2016 1350.00 30
     * Sat Oct 01 00:00:00 BST 2016 Mon Oct 31 23:59:59 GMT 2016 1350.00 31
     * Tue Nov 01 00:00:00 GMT 2016 Wed Nov 30 23:59:59 GMT 2016 1350.00 30
     * Thu Dec 01 00:00:00 GMT 2016 Sat Dec 31 23:59:59 GMT 2016 1350.00 31
     */
    @Test
    public void testCaseThree() throws ParseException {

        String rentAmount = "1350.00";

        PaymentSchedule schedule = getSchedule(rentAmount,"Monthly", "1 Feb 2016","31 Dec 2016","Monthly");

        List<Item> items = schedule.getItems();
        for (Item item : items) {
            System.out.println(item.getStart() + " " + item.getEnd() + " " + getRate(item) + " " + item.getDaysInPeriod());
        }

        Assert.assertEquals(11, items.size());

        //Feb
        Assert.assertEquals(29, items.get(0).getDaysInPeriod());
        Assert.assertEquals(rentAmount, getRate(items.get(0)));

        //March
        Assert.assertEquals(31, items.get(1).getDaysInPeriod());
        Assert.assertEquals(rentAmount, getRate(items.get(1)));

        //April
        Assert.assertEquals(30, items.get(2).getDaysInPeriod());
        Assert.assertEquals(rentAmount, getRate(items.get(2)));

        //May
        Assert.assertEquals(31, items.get(3).getDaysInPeriod());
        Assert.assertEquals(rentAmount, getRate(items.get(3)));

        //June
        Assert.assertEquals(30, items.get(4).getDaysInPeriod());
        Assert.assertEquals(rentAmount, getRate(items.get(4)));

        //July
        Assert.assertEquals(31, items.get(5).getDaysInPeriod());
        Assert.assertEquals(rentAmount, getRate(items.get(5)));

        //Aug
        Assert.assertEquals(31, items.get(6).getDaysInPeriod());
        Assert.assertEquals(rentAmount, getRate(items.get(6)));

        //Sep
        Assert.assertEquals(30, items.get(7).getDaysInPeriod());
        Assert.assertEquals(rentAmount, getRate(items.get(7)));

        //Oct
        Assert.assertEquals(31, items.get(8).getDaysInPeriod());
        Assert.assertEquals(rentAmount, getRate(items.get(8)));

        //Nov
        Assert.assertEquals(30, items.get(9).getDaysInPeriod());
        Assert.assertEquals(rentAmount, getRate(items.get(9)));

        //Dec
        Assert.assertEquals(31, items.get(10).getDaysInPeriod());
        Assert.assertEquals(rentAmount, getRate(items.get(10)));

        Assert.assertEquals("14850.00", getTotalCharge(schedule));
    }

    /**
     * Sat Jan 30 00:00:00 GMT 2016 Sun Feb 28 23:59:59 GMT 2016 1350.00 30
     * Mon Feb 29 00:00:00 GMT 2016 Tue Mar 29 23:59:59 BST 2016 1350.00 30
     * Wed Mar 30 00:00:00 BST 2016 Fri Apr 29 23:59:59 BST 2016 1350.00 31
     * Sat Apr 30 00:00:00 BST 2016 Sun May 29 23:59:59 BST 2016 1350.00 30
     * Mon May 30 00:00:00 BST 2016 Wed Jun 29 23:59:59 BST 2016 1350.00 31
     * Thu Jun 30 00:00:00 BST 2016 Fri Jul 29 23:59:59 BST 2016 1350.00 30
     * Sat Jul 30 00:00:00 BST 2016 Mon Aug 29 23:59:59 BST 2016 1350.00 31
     * Tue Aug 30 00:00:00 BST 2016 Thu Sep 29 23:59:59 BST 2016 1350.00 31
     * Fri Sep 30 00:00:00 BST 2016 Sat Oct 29 23:59:59 BST 2016 1350.00 30
     * Sun Oct 30 00:00:00 BST 2016 Tue Nov 29 23:59:59 GMT 2016 1350.00 31
     * Wed Nov 30 00:00:00 GMT 2016 Thu Dec 29 23:59:59 GMT 2016 1350.00 30
     * Fri Dec 30 00:00:00 GMT 2016 Sun Jan 29 23:59:59 GMT 2017 1350.00 31
     */
    @Test
    public void testCaseFour() throws ParseException {

        String rentAmount = "1350.00";

        PaymentSchedule schedule = getSchedule(rentAmount,"Monthly", "30 Jan 2016","29 Jan 2017","Monthly");
        List<Item> items = schedule.getItems();
        for (Item item : items) {
            System.out.println(item.getStart() + " " + item.getEnd() + " " + getRate(item) + " " + item.getDaysInPeriod());
        }

        Assert.assertEquals(12, items.size());

        //Jan
        Assert.assertEquals(30, items.get(0).getDaysInPeriod());
        Assert.assertEquals(rentAmount, getRate(items.get(0)));

        //Feb
        Assert.assertEquals(30, items.get(1).getDaysInPeriod());
        Assert.assertEquals(rentAmount, getRate(items.get(1)));

        //Mar
        Assert.assertEquals(31, items.get(2).getDaysInPeriod());
        Assert.assertEquals(rentAmount, getRate(items.get(2)));

        //April
        Assert.assertEquals(30, items.get(3).getDaysInPeriod());
        Assert.assertEquals(rentAmount, getRate(items.get(3)));

        //May
        Assert.assertEquals(31, items.get(4).getDaysInPeriod());
        Assert.assertEquals(rentAmount, getRate(items.get(4)));

        //June
        Assert.assertEquals(30, items.get(5).getDaysInPeriod());
        Assert.assertEquals(rentAmount, getRate(items.get(5)));

        //July
        Assert.assertEquals(31, items.get(6).getDaysInPeriod());
        Assert.assertEquals(rentAmount, getRate(items.get(6)));

        //Aug
        Assert.assertEquals(31, items.get(7).getDaysInPeriod());
        Assert.assertEquals(rentAmount, getRate(items.get(7)));

        //Sep
        Assert.assertEquals(30, items.get(8).getDaysInPeriod());
        Assert.assertEquals(rentAmount, getRate(items.get(8)));

        //Oct
        Assert.assertEquals(31, items.get(9).getDaysInPeriod());
        Assert.assertEquals(rentAmount, getRate(items.get(9)));

        //Nov
        Assert.assertEquals(30, items.get(10).getDaysInPeriod());
        Assert.assertEquals(rentAmount, getRate(items.get(10)));

        //Dec
        Assert.assertEquals(31, items.get(11).getDaysInPeriod());
        Assert.assertEquals(rentAmount, getRate(items.get(11)));

        Assert.assertEquals("", getTotalCharge(schedule));
    }

    /**
     * Sun Jan 31 00:00:00 GMT 2016 Sun Feb 28 23:59:59 GMT 2016 1350.00 29
     * Mon Feb 29 00:00:00 GMT 2016 Wed Mar 30 23:59:59 BST 2016 1350.00 31
     * Thu Mar 31 00:00:00 BST 2016 Fri Apr 29 23:59:59 BST 2016 1350.00 30
     * Sat Apr 30 00:00:00 BST 2016 Mon May 30 23:59:59 BST 2016 1350.00 31
     * Tue May 31 00:00:00 BST 2016 Wed Jun 29 23:59:59 BST 2016 1350.00 30
     * Thu Jun 30 00:00:00 BST 2016 Sat Jul 30 23:59:59 BST 2016 1350.00 31
     * Sun Jul 31 00:00:00 BST 2016 Tue Aug 30 23:59:59 BST 2016 1350.00 31
     * Wed Aug 31 00:00:00 BST 2016 Thu Sep 29 23:59:59 BST 2016 1350.00 30
     * Fri Sep 30 00:00:00 BST 2016 Sun Oct 30 23:59:59 GMT 2016 1350.00 31
     * Mon Oct 31 00:00:00 GMT 2016 Tue Nov 29 23:59:59 GMT 2016 1350.00 30
     * Wed Nov 30 00:00:00 GMT 2016 Fri Dec 30 23:59:59 GMT 2016 1350.00 31
     * Sat Dec 31 00:00:00 GMT 2016 Mon Jan 30 23:59:59 GMT 2017 1350.00 31
     */
    @Test
    public void testCaseFive() throws ParseException {


        String rentAmount = "1350.00";

        PaymentSchedule schedule = getSchedule(rentAmount,"Monthly", "31 Jan 2016","30 Jan 2017","Monthly");

        List<Item> items = schedule.getItems();
        for (Item item : items) {
            System.out.println(item.getStart() + " " + item.getEnd() + " " + getRate(item) + " " + item.getDaysInPeriod());
        }

        Assert.assertEquals(12, items.size());

        //Jan
        Assert.assertEquals(29, items.get(0).getDaysInPeriod());
        Assert.assertEquals(rentAmount, getRate(items.get(0)));

        //Feb
        Assert.assertEquals(31, items.get(1).getDaysInPeriod());
        Assert.assertEquals(rentAmount, getRate(items.get(1)));

        //Mar
        Assert.assertEquals(30, items.get(2).getDaysInPeriod());
        Assert.assertEquals(rentAmount, getRate(items.get(2)));

        //April
        Assert.assertEquals(31, items.get(3).getDaysInPeriod());
        Assert.assertEquals(rentAmount, getRate(items.get(3)));

        //May
        Assert.assertEquals(30, items.get(4).getDaysInPeriod());
        Assert.assertEquals(rentAmount, getRate(items.get(4)));

        //June
        Assert.assertEquals(31, items.get(5).getDaysInPeriod());
        Assert.assertEquals(rentAmount, getRate(items.get(5)));

        //July
        Assert.assertEquals(31, items.get(6).getDaysInPeriod());
        Assert.assertEquals(rentAmount, getRate(items.get(6)));

        //Aug
        Assert.assertEquals(30, items.get(7).getDaysInPeriod());
        Assert.assertEquals(rentAmount, getRate(items.get(7)));

        //Sep
        Assert.assertEquals(31, items.get(8).getDaysInPeriod());
        Assert.assertEquals(rentAmount, getRate(items.get(8)));

        //Oct
        Assert.assertEquals(30, items.get(9).getDaysInPeriod());
        Assert.assertEquals(rentAmount, getRate(items.get(9)));

        //Nov
        Assert.assertEquals(31, items.get(10).getDaysInPeriod());
        Assert.assertEquals(rentAmount, getRate(items.get(10)));

        //Dec
        Assert.assertEquals(31, items.get(11).getDaysInPeriod());
        Assert.assertEquals(rentAmount, getRate(items.get(11)));

        Assert.assertEquals("16200.00", getTotalCharge(schedule));
    }

    /**
     * Fri Jan 01 00:00:00 GMT 2016 Sun Jan 31 23:59:59 GMT 2016 1550.00 31
     * Mon Feb 01 00:00:00 GMT 2016 Mon Feb 29 23:59:59 GMT 2016 1450.00 29
     * Tue Mar 01 00:00:00 GMT 2016 Thu Mar 31 23:59:59 BST 2016 1550.00 31
     * Fri Apr 01 00:00:00 BST 2016 Sat Apr 30 23:59:59 BST 2016 1500.00 30
     * Sun May 01 00:00:00 BST 2016 Tue May 31 23:59:59 BST 2016 1550.00 31
     * Wed Jun 01 00:00:00 BST 2016 Thu Jun 30 23:59:59 BST 2016 1500.00 30
     * Fri Jul 01 00:00:00 BST 2016 Sun Jul 31 23:59:59 BST 2016 1550.00 31
     * Mon Aug 01 00:00:00 BST 2016 Wed Aug 31 23:59:59 BST 2016 1550.00 31
     * Thu Sep 01 00:00:00 BST 2016 Fri Sep 30 23:59:59 BST 2016 1500.00 30
     * Sat Oct 01 00:00:00 BST 2016 Mon Oct 31 23:59:59 GMT 2016 1550.00 31
     * Tue Nov 01 00:00:00 GMT 2016 Wed Nov 30 23:59:59 GMT 2016 1500.00 30
     * Thu Dec 01 00:00:00 GMT 2016 Sat Dec 31 23:59:59 GMT 2016 1550.00 31
     */
    @Test
    public void testCaseSix() throws ParseException {


        PaymentSchedule schedule = getSchedule("350","Weekly", "1 Jan 2016","31 Dec 2016","Monthly");

        List<Item> items = schedule.getItems();
        for (Item item : items) {
            System.out.println(item.getStart() + " " + item.getEnd() + " " + getRate(item) + " " + item.getDaysInPeriod());
        }

        Assert.assertEquals(12, items.size());

        //Jan
        Assert.assertEquals(31, items.get(0).getDaysInPeriod());
        Assert.assertEquals("1550.00", getRate( items.get(0)));

        //Feb
        Assert.assertEquals(29, items.get(1).getDaysInPeriod());
        Assert.assertEquals("1450.00", getRate(items.get(1)));

        //Mar
        Assert.assertEquals(31, items.get(2).getDaysInPeriod());
        Assert.assertEquals("1550.00", getRate( items.get(2)));

        //April
        Assert.assertEquals(30, items.get(3).getDaysInPeriod());
        Assert.assertEquals("1500.00", getRate(items.get(3)));

        //May
        Assert.assertEquals(31, items.get(4).getDaysInPeriod());
        Assert.assertEquals("1550.00", getRate( items.get(4)));

        //June
        Assert.assertEquals(30, items.get(5).getDaysInPeriod());
        Assert.assertEquals("1500.00", getRate(items.get(5)));

        //July
        Assert.assertEquals(31, items.get(6).getDaysInPeriod());
        Assert.assertEquals("1550.00", getRate( items.get(6)));

        //Aug
        Assert.assertEquals(31, items.get(7).getDaysInPeriod());
        Assert.assertEquals("1550.00", getRate( items.get(7)));

        //Sep
        Assert.assertEquals(30, items.get(8).getDaysInPeriod());
        Assert.assertEquals("1500.00", getRate(items.get(8)));

        //Oct
        Assert.assertEquals(31, items.get(9).getDaysInPeriod());
        Assert.assertEquals("1550.00", getRate( items.get(9)));

        //Nov
        Assert.assertEquals(30, items.get(10).getDaysInPeriod());
        Assert.assertEquals("1500.00", getRate(items.get(10)));

        //Dec
        Assert.assertEquals(31, items.get(11).getDaysInPeriod());
        Assert.assertEquals("1550.00", getRate( items.get(11)));

        Assert.assertEquals("18300.00", getTotalCharge(schedule));
    }

    /**
     * Fri Jan 01 00:00:00 GMT 2016 Thu Mar 31 23:59:59 BST 2016 4050.00 91
     * Fri Apr 01 00:00:00 BST 2016 Thu Jun 30 23:59:59 BST 2016 4050.00 91
     * Fri Jul 01 00:00:00 BST 2016 Fri Sep 30 23:59:59 BST 2016 4050.00 92
     * Sat Oct 01 00:00:00 BST 2016 Sat Dec 31 23:59:59 GMT 2016 4050.00 92
     */
    @Test
    public void testCasSeven() throws ParseException {

        String rentAmount = "1350.00";

        PaymentSchedule schedule = getSchedule(rentAmount,"Monthly", "1 Jan 2016","31 Dec 2016","Quarterly");

        List<Item> items = schedule.getItems();
        for (Item item : items) {
            System.out.println(item.getStart() + " " + item.getEnd() + " " + getRate(item) + " " + item.getDaysInPeriod());
        }

        Assert.assertEquals(4, items.size());

        //One
        Assert.assertEquals(91, items.get(0).getDaysInPeriod());
        Assert.assertEquals("4050.00", getRate(items.get(0)));

        //Two
        Assert.assertEquals(91, items.get(1).getDaysInPeriod());
        Assert.assertEquals("4050.00", getRate(items.get(1)));

        //Three
        Assert.assertEquals(92, items.get(2).getDaysInPeriod());
        Assert.assertEquals("4050.00", getRate(items.get(2)));

        //Four
        Assert.assertEquals(92, items.get(3).getDaysInPeriod());
        Assert.assertEquals("4050.00", getRate(items.get(3)));

        Assert.assertEquals("16200.00", getTotalCharge(schedule));
    }

}
